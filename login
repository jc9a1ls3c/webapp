<%@ Page Title="eCommerce Application - Login" Language="C#" MasterPageFile="~/eCommerceApplication.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="LoginHeadContent" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="LoginHeaderRightContent" ContentPlaceHolderID="HeaderRightCorner" runat="Server">
    <li class="a text-left">Customer</li>
    <li><a href="Login.aspx">Login</a></li>
</asp:Content>
<asp:Content ID="LoginMainContent" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
    <div class="container">
        <div class="main">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Login</h2>
                </div>
            </div>
            <form id="LoginForm" runat="server" class="form-horizontal">
                <div class="form-group">
                    <label for="inputmane3" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" autocomplete="off"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control" autocomplete="off"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" CssClass="btn btn-default" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:PlaceHolder ID="ErrorPlaceHolder" runat="server" Visible="False">
                            <div class="alert alert-dismissable alert-danger">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <asp:Label ID="txtMessage" runat="server" Font-Bold="True" ForeColor="Red" CssClass="alert-link"></asp:Label>
                            </div>
                        </asp:PlaceHolder>
                    </div>
                </div>
            </form>
        </div>
    </div>
</asp:Content>
